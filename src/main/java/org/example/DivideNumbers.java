package org.example;

public class DivideNumbers {

        public static void main(String[] args) {
            DivideNumbers.AddTwoNumbers(10,2);

        }

        public  static void AddTwoNumbers(int numerator, int denominator){

            if (denominator != 0) {
                // Perform division.
                int quotient = numerator / denominator;

                // Print the result
                System.out.println("Quotient: " + quotient);
            } else {
                System.out.println("Error: Division by zero is not allowed.");
            }

        }
    }


