package org.example;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestDivideNumbers {
        @Test
        public void testDivisionWithNonZeroDenominator() {
            int numerator = 10;
            int denominator = 2;

            int expectedQuotient = 5;

            int actualQuotient = numerator / denominator;

            assertEquals(expectedQuotient, actualQuotient);
        }

        @Test
        public void testDivisionWithZeroDenominator() {
            int numerator = 10;
            int denominator = 0;

            try {
                int quotient = numerator / denominator;

                fail("Expected ArithmeticException to be thrown");
            } catch (ArithmeticException e) {
                // Exception is expected, test passes
            }
        }
    }


